using System.Threading.Tasks;
using Contracts.Repository;
using Entities_;
using Repository.LogicRepositories;

namespace Repository
{
    public class RepositoryManager: IRepositoryManager
    {
        private RepositoryContext _context;
        private ICategoryRepository _categoryRepository;
        private IBlogRepository _blogRepository;

        public RepositoryManager(RepositoryContext context)
        {
            _context = context;
        }

        public ICategoryRepository Category
        {
            get
            {
                if(_categoryRepository == null)
                    _categoryRepository = new CategoryRepository(_context);
                return _categoryRepository;
            }
        }

        public IBlogRepository Blog
        {
            get
            {
                if(_blogRepository == null)
                    _blogRepository = new BlogRepository(_context);
                return _blogRepository;
            }
        }

        public Task Save() => _context.SaveChangesAsync();
    }
}
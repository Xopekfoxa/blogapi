using System;
using System.Linq;
using System.Reflection;
using System.Text;
using Entities_.Models;

namespace Repository.Extensions.Utility
{
    public static class OrderQueryBuilder
    {
        public static string CreateOderQuery<T>(string orderBy)
        {
            var orderParams = orderBy.Trim().Split(',');
            var propertyInfos = typeof(Blog).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            
            var orderQueryBuilder = new StringBuilder();

            foreach (var item in orderParams)
            {
                if(string.IsNullOrWhiteSpace(item))
                    continue;

                var propertyFromQuery = item.Split(" ")[0];
                var objectProperty = propertyInfos.FirstOrDefault(p =>
                    p.Name.Equals(propertyFromQuery, StringComparison.CurrentCultureIgnoreCase));

                if (objectProperty == null)
                    continue;

                var direction = item.EndsWith(" desc") ? "descending" : "ascending";
                orderQueryBuilder.Append($"{objectProperty.Name.ToString()} {direction},");
                
            }

            var orderQuery = orderQueryBuilder.ToString().TrimEnd(',', ' ');
            return orderQuery;
        }
    }
}
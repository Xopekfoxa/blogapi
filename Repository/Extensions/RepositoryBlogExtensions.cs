using System;
using System.Linq;
using Entities_.Models;
using System.Linq.Dynamic.Core;
using System.Reflection;
using System.Text;
using Repository.Extensions.Utility;

namespace Repository.Extensions
{
    public static class RepositoryBlogExtensions
    {
        public static IQueryable<Blog> Search(this IQueryable<Blog> blogs,
            string searchTerm)
        {
            if (string.IsNullOrWhiteSpace(searchTerm))
                return blogs;
            var lowerCase = searchTerm.Trim().ToLower();

            return blogs.Where(e => e.Title.ToLower().Contains(lowerCase));
        }

        public static IQueryable<Blog> Sort(this IQueryable<Blog> blogs,
            string orderBy)
        {
            if (string.IsNullOrWhiteSpace(orderBy))
                return blogs.OrderBy(e => e.Title);

            var orderQuery = OrderQueryBuilder.CreateOderQuery<Blog>(orderBy);
            
            if (string.IsNullOrWhiteSpace(orderQuery))
                return blogs.OrderBy(e => e.Title);

            return blogs.OrderBy(orderQuery);
        }
    }
}
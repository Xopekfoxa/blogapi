using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.Repository;
using Entities_;
using Entities_.Models;
using Microsoft.EntityFrameworkCore;

namespace Repository.LogicRepositories
{
    public class CategoryRepository: RepositoryBase<Category>,ICategoryRepository
    {
        public CategoryRepository(RepositoryContext context) : base(context)
        {
            
        }

        public async Task<IEnumerable<Category>> GetAllCategory(bool trackChanges) =>
            await FindAll(trackChanges)
                .OrderBy(c => c.Name)
                .ToListAsync();

        public async Task<Category> GetCategory(int catId, bool trackChanges) =>
            await FindByCondition(c => c.Id.Equals(catId), trackChanges)
                .SingleOrDefaultAsync();

        public void CreateCategory(Category category) => Create(category);

        public async Task<IEnumerable<Category>> GetByIds(IEnumerable<int> ids, bool trackChanges) =>
            await FindByCondition(x => ids.Contains(x.Id), trackChanges)
                .ToListAsync();

        public void DeleteCategory(Category cat) => Delete(cat);
    }
}
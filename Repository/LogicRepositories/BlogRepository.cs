using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.Repository;
using Entities_;
using Entities_.Models;
using Entities_.RequestFeatures;
using Microsoft.EntityFrameworkCore;
using Repository.Extensions;

namespace Repository.LogicRepositories
{
    public class BlogRepository : RepositoryBase<Blog>,IBlogRepository
    {
        public BlogRepository(RepositoryContext context) : base(context)
        {
            
        }

        public async Task<IEnumerable<Blog>> GetAllBlogs(bool trackChanges) =>
            await FindAll(trackChanges)
                .Include(p => p.Category)
                .OrderBy(p => p.Title)
                .ToListAsync();
        
        public async Task<PagedList<Blog>> GetAllBlogs(bool trackChanges, BlogParameters parameters)
        {
            var blogs = await FindAll(trackChanges)
                .Include(p => p.Category)
                .Search(parameters.SearchTerm)
                .Sort(parameters.OrderBy)
                .ToListAsync();
            
            return PagedList<Blog>
                .ToPagedList(blogs,parameters.PageNumber,parameters.PageSize);
        }

        public async Task<Blog> GetBlog(int blogId, bool trackChanges) =>
            await FindByCondition(p => p.Id == blogId, trackChanges)
                .Include(p => p.Category)
                .SingleOrDefaultAsync();

        public async Task<IEnumerable<Blog>> GetBlogsForCategory(int catId, bool trackChanges) =>
            await FindByCondition(p => p.CategoryId == catId, trackChanges)
                .Include(p => p.Category)
                .ToListAsync();

        public async Task<PagedList<Blog>> GetBlogsForCategory(int catId,
            BlogParameters parameters, bool trackChanges)
        {
            var blogs = await FindByCondition(p => p.CategoryId == catId, trackChanges)
                .Search(parameters.SearchTerm)
                .OrderBy(e => e.Title)
                .ToListAsync();
            
            return PagedList<Blog>
                .ToPagedList(blogs,parameters.PageNumber,parameters.PageSize);
        }
        
        public void CreateBlogForBlog(int catId, Blog blog)
        {
            blog.CategoryId = catId;
            Create(blog);
        }

        public void DeleteBlog(Blog blog) => Delete(blog);
    }
}
﻿using Contracts;
using NLog;

namespace LoggerService
{
    public class LoggerManager : ILoggerManager
    {
        private static readonly ILogger Logger = LogManager.GetCurrentClassLogger();
        
        public void LogMessage(string mes)
        {
            Logger.Info(mes);
        }

        public void LogDebug(string mes)
        {
            Logger.Debug(mes);
        }

        public void LogWarning(string mes)
        {
            Logger.Warn(mes);
        }

        public void LogError(string mes)
        {
            Logger.Error(mes);
        }
    }
}
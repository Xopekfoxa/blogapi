using AutoMapper;
using Entities_.DataTransferObjects;
using Entities_.DataTransferObjects.BlogDTOs;
using Entities_.DataTransferObjects.CategoryDTOs;
using Entities_.Models;

namespace BlogAPI
{
    public class MappingProfile:Profile
    {
        public MappingProfile()
        {
            CreateMap<Category, CategoryDto>();
            CreateMap<CategoryForCreationDto, Category>();
            CreateMap<CategoryForUpdateDto, Category>();

            CreateMap<Blog, BlogDto>();
            CreateMap<BlogForCreactionDto, Blog>();
            CreateMap<BlogForUpdateDto, Blog>().ReverseMap();
        }
    }
}
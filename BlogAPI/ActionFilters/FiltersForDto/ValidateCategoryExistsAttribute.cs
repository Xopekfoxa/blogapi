using System.Threading.Tasks;
using Contracts;
using Contracts.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;

namespace BlogAPI.ActionFilters.FiltersForDto
{
    public class ValidateCategoryExistsAttribute: IAsyncActionFilter
    {
        private readonly IRepositoryManager _repository;
        private readonly ILoggerManager _logger;

        public ValidateCategoryExistsAttribute(IRepositoryManager manager,ILoggerManager logger)
        {
            _repository = manager;
            _logger = logger;
        }
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var trackChanges = context.HttpContext.Request.Method.Equals("PUT") ? true : false;
            var id = (int)context.ActionArguments["id"];
            var category = await _repository.Category.GetCategory(id, trackChanges);

            if (category == null)
            {
                _logger.LogMessage($"Category with id: {id} doesn't exist in the database.");
                context.Result = new NotFoundResult();
            }
            else
            {
                context.HttpContext.Items.Add("category",category);
                await next();
            }
        }
    }
}
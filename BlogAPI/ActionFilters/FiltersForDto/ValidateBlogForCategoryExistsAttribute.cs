using System.Threading.Tasks;
using Contracts;
using Contracts.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace BlogAPI.ActionFilters.FiltersForDto
{
    public class ValidateBlogForCategoryExistsAttribute : IAsyncActionFilter
    {
        private readonly IRepositoryManager _repository;
        private readonly ILoggerManager _logger;

        public ValidateBlogForCategoryExistsAttribute(IRepositoryManager repository,
            ILoggerManager logger)
        {
            _repository = repository;
            _logger = logger;
        }
        
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var method = context.HttpContext.Request.Method;
            bool trackChanges = (method.Equals("PUT") || method.Equals("PATCH")) ? true : false;

            int categoryId = (int) context.ActionArguments["catId"];
            var category = await _repository.Category.GetCategory(categoryId, false);

            if (category == null)
            {
                _logger.LogMessage($"Category with id: {categoryId} doesn't exist in the database");
                context.Result = new NotFoundResult();
                return;
            }
                
            int id = (int)context.ActionArguments["id"];
            var blog = await _repository.Blog.GetBlog(id, trackChanges);

            if (blog == null)
            {
                _logger.LogMessage($"Blog with id: {id} doesn't exist in the database.");
                context.Result = new NotFoundResult();
            }
            else
            {
                if (categoryId != blog.CategoryId)
                {
                    blog.Category = category;
                }
                context.HttpContext.Items.Add("blog",blog);
                await next();
            }

        }
    }
}
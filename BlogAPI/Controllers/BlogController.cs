using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BlogAPI.ActionFilters;
using BlogAPI.ActionFilters.FiltersForDto;
using Contracts;
using Contracts.Repository;
using Entities_.DataTransferObjects;
using Entities_.DataTransferObjects.BlogDTOs;
using Entities_.DataTransferObjects.CategoryDTOs;
using Entities_.Models;
using Entities_.RequestFeatures;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace BlogAPI.Controllers
{
    [Route("api/blogs")]
    [ApiController]
    public class BlogController: Controller
    {
        private readonly IRepositoryManager _repository;
        private readonly ILoggerManager _logger;
        private readonly IMapper _mapper;

        public BlogController(IRepositoryManager manager, ILoggerManager logger, IMapper mapper)
        {
            _repository = manager;
            _logger = logger;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetBlogs([FromQuery] BlogParameters blogParameters)
        {
            var blogs = await  _repository.Blog.GetAllBlogs( false,blogParameters);
            Response.Headers.Add("X-Pagination",JsonConvert.SerializeObject(blogs.MetaData));
            var blogsDto = _mapper.Map<IEnumerable<BlogDto>>(blogs);
            
            return Ok(blogsDto);
        }
        
        [HttpGet("{id}",Name="BlogById")]
        public async Task<IActionResult> GetBlog(int id)
        {
            var blog = await _repository.Blog.GetBlog(id, false);

            if (blog == null)
            {
                _logger.LogMessage($"Blog with id: {id} doesn't exist in the database.");
                return NotFound();
            }
            else
            {
                var blogDto = _mapper.Map<BlogDto>(blog);
                return Ok(blogDto);
            }
        }

        [HttpGet("categories/{id}")]
        [ServiceFilter(typeof(ValidateCategoryExistsAttribute))]
        public async Task<IActionResult> GetBlogsForCategory(int id,[FromQuery] BlogParameters blogParameters)
        {
            var cat = HttpContext.Items["category"] as Category; 
            
            var blogs = await _repository.Blog.GetBlogsForCategory(cat.Id,blogParameters, false);
            Response.Headers.Add("X-Pagination",JsonConvert.SerializeObject(blogs.MetaData));
            var blogsDto = _mapper.Map<IEnumerable<BlogDto>>(blogs);
            return Ok(blogsDto);
            
        }
        
        [HttpPost("categories/{id}")]
        [ServiceFilter(typeof(ValidationFilterAttribute))]
        [ServiceFilter(typeof(ValidateCategoryExistsAttribute))]
        public async Task<IActionResult> CreateBlogForCategory(int id, [FromBody] BlogForCreactionDto blog)
        {
            var category = HttpContext.Items["category"] as Category; 

            var blogEntity = _mapper.Map<Blog>(blog);
            
            _repository.Blog.CreateBlogForBlog(id,blogEntity);
            await _repository.Save();

            var blogReturn = _mapper.Map<BlogDto>(blogEntity);
            blogReturn.Category = _mapper.Map<CategoryDto>(category);

            return CreatedAtRoute("BlogById", new {id = blogReturn.Id}, blogReturn);
        }

        [HttpDelete("{id}/categories/{catId}")]
        [ServiceFilter(typeof(ValidateBlogForCategoryExistsAttribute))]
        public async Task<IActionResult> DeleteBlog(int id)
        {
            Blog blog = HttpContext.Items["blog"] as Blog;
            
            _repository.Blog.DeleteBlog(blog);
            await _repository.Save();

            return NoContent();
        }

        [HttpPut("{id}/categories/{catId}")]
        [ServiceFilter(typeof(ValidationFilterAttribute))]
        [ServiceFilter(typeof(ValidateBlogForCategoryExistsAttribute))]
        public async Task<IActionResult> UpdateBlogForCategory(int catId, int id,
            [FromBody] BlogForUpdateDto blog)
        {
            var blogEntity = HttpContext.Items["blog"] as Blog;
           
            _mapper.Map(blog, blogEntity);
            
            await _repository.Save();
            
            return NoContent();
        }

        [HttpPatch("{id}/categories/{catId}")]
        [ServiceFilter(typeof(ValidateBlogForCategoryExistsAttribute))]
        public async Task<IActionResult> PartialUpdateBlogForCategory(int catId, int id,
            [FromBody] JsonPatchDocument<BlogForUpdateDto> patchDoc)
        {
            if (patchDoc == null)
            {
                _logger.LogError("patchDoc object sent from client is null.");
                return BadRequest("pathDoc object is null.");
            }
           
            var blogEntity = HttpContext.Items["blog"] as Blog;

            var blogPatch = _mapper.Map<BlogForUpdateDto>(blogEntity);
            patchDoc.ApplyTo(blogPatch);

            TryValidateModel(blogPatch);
            
            if (!ModelState.IsValid)
            {
                _logger.LogError("Invalid model state for the BlogForUpdateDto object");
                return UnprocessableEntity(ModelState);
            }
            
            _mapper.Map(blogPatch, blogEntity);

            await _repository.Save();

            return NoContent();
        }
    }
}
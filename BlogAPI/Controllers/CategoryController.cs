using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using BlogAPI.ActionFilters;
using BlogAPI.ActionFilters.FiltersForDto;
using BlogAPI.ModelBinders;
using Contracts;
using Contracts.Repository;
using Entities_.DataTransferObjects;
using Entities_.DataTransferObjects.BlogDTOs;
using Entities_.DataTransferObjects.CategoryDTOs;
using Entities_.Models;
using Microsoft.AspNetCore.Mvc;
using Entities_.RequestFeatures;
using Newtonsoft.Json;
using NLog;

namespace BlogAPI.Controllers
{
    [Route("api/categories")]
    [ApiController]
    public class CategoryController : Controller
    {
        private readonly IRepositoryManager _repository;
        private readonly ILoggerManager _logger;
        private readonly IMapper _mapper;

        public CategoryController(IRepositoryManager repository, ILoggerManager logger, IMapper mapper)
        {
            _repository = repository;
            _logger = logger;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetCategories()
        {
            var cat = await _repository.Category.GetAllCategory(false);

            var categoryDto = _mapper.Map<IEnumerable<CategoryDto>>(cat);
                
            return Ok(categoryDto);
        }

        [HttpGet("{id}",Name="CategoryById")]
        public async Task<IActionResult> GetCategory(int id)
        {
            var cat = await _repository.Category.GetCategory(id, false);
            if (cat == null)
            {
                _logger.LogMessage($"Category with id: {id} doesn't exist in the database.");
                return NotFound();
            }
            else
            {
                var catDto = _mapper.Map<CategoryDto>(cat);
                return Ok(catDto);
            }
        }

        [HttpGet("{id}/blogs")]
        [ServiceFilter(typeof(ValidateCategoryExistsAttribute))]
        public async Task<IActionResult> GetBlogsForCategory(int id,[FromQuery]
            BlogParameters blogParameters)
        {
            var cat = HttpContext.Items["category"] as Category; 
           
            var blogs = await  _repository.Blog.GetBlogsForCategory(cat.Id, blogParameters,false);
            Response.Headers.Add("X-Pagination",JsonConvert.SerializeObject(blogs.MetaData));
            var blogsDto = _mapper.Map<IEnumerable<BlogDto>>(blogs);
            return Ok(blogsDto);
        }

        [HttpPost]
        [ServiceFilter(typeof(ValidationFilterAttribute))]
        public async Task<IActionResult> CreateCategory([FromBody] CategoryForCreationDto category)
        {
            var categoryEntity = _mapper.Map<Category>(category);
            
            _repository.Category.CreateCategory(categoryEntity);
            await _repository.Save();

            var categoryReturn = _mapper.Map<CategoryDto>(categoryEntity);

            return CreatedAtRoute("CategoryById", new {id = categoryReturn.Id}, categoryReturn);
        }

        [HttpGet("collection/({ids})",Name="GetCategoryCollection")]
        public async Task<IActionResult> CreateCategoryCollection([ModelBinder(BinderType = 
            typeof(ArrayModelBinder))]IEnumerable<int> ids)
        {
            if (ids == null)
            {
                _logger.LogError("Parameter ids is null!");
                return BadRequest("Parameter ids is null");
            }

            var catEntities = await _repository.Category.GetByIds(ids,false);
            if (ids.Count() != catEntities.Count())
            {
                _logger.LogMessage("Some ids are not valid in a collection");
                return NotFound();
            }

            var catReturn = _mapper.Map<IEnumerable<CategoryDto>>(catEntities);
            return Ok(catReturn);
        }
       
        [HttpPost("collection")]
        public async Task<IActionResult>  CreateCategoryCollection([FromBody] IEnumerable<CategoryForCreationDto> categories)
        {
            if (categories == null)
            {
                _logger.LogError("Category collection sent from client is null");
                return BadRequest("Category collection is null");
            }
            
            if (!ModelState.IsValid)
            {
                _logger.LogError("Invalid model state for the CategoryForCreationDto object");
                return UnprocessableEntity(ModelState);
            }

            var categoryEntities = _mapper.Map<IEnumerable<Category>>(categories);

            foreach (var item in categoryEntities)
            {
                _repository.Category.CreateCategory(item);
            }
            
            await _repository.Save();

            var categoryCollReturn = _mapper.Map<IEnumerable<CategoryDto>>(categoryEntities);
            var ids = string.Join(",", categoryCollReturn.Select(c => c.Id));

            return CreatedAtRoute("GetCategoryCollection", new {ids}, categoryCollReturn);
        }
        
        [HttpDelete("{id}")]
        [ServiceFilter(typeof(ValidateCategoryExistsAttribute))]
        public async Task<IActionResult> DeleteCategory(int id)
        {
            Category cat = HttpContext.Items["category"] as Category; 
            
            _repository.Category.DeleteCategory(cat);
            await _repository.Save();

            return NoContent();
        }

        [HttpPut("{id}")]
        [ServiceFilter(typeof(ValidationFilterAttribute))]
        [ServiceFilter(typeof(ValidateCategoryExistsAttribute))]
        public async Task<IActionResult>  UpdateCategory(int id, [FromBody] CategoryForUpdateDto category)
        {
            var categoryEntity = HttpContext.Items["category"] as Category;

            _mapper.Map(category, categoryEntity);
            await _repository.Save();

            return NoContent();
        }
        
        
    }
}
﻿namespace Contracts
{
    public interface ILoggerManager
    {
         void LogMessage(string mes);
         void LogDebug(string mes);
         void LogWarning(string mes);
         void LogError(string mes);
    }
}
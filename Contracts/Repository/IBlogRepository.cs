using System.Collections.Generic;
using System.Threading.Tasks;
using Entities_.Models;
using Entities_.RequestFeatures;

namespace Contracts.Repository
{
    public interface IBlogRepository
    {
        Task<IEnumerable<Blog>> GetAllBlogs(bool trackChanges);
        Task<PagedList<Blog>> GetAllBlogs(bool trackChanges,BlogParameters blogParameters);
        Task<Blog> GetBlog(int blogId, bool trackChanges);
        Task<IEnumerable<Blog>> GetBlogsForCategory(int id,bool trackChanges);
        Task<PagedList<Blog>> GetBlogsForCategory(int id,BlogParameters blogParameters, bool trackChanges);
        void CreateBlogForBlog(int catId, Blog blog);
        void DeleteBlog(Blog blog);
    }
}
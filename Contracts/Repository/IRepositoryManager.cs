using System.Threading.Tasks;

namespace Contracts.Repository
{
    public interface IRepositoryManager
    {
        ICategoryRepository Category { get;  }
        IBlogRepository Blog { get;  }
        Task Save();
    }
}
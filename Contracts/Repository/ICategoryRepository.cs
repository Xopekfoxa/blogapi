using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Entities_.Models;

namespace Contracts.Repository
{
    public interface ICategoryRepository
    {
        Task<IEnumerable<Category>> GetAllCategory(bool trackChanges);
        Task<Category> GetCategory(int catId, bool trackChanges);
        void CreateCategory(Category category);
        Task<IEnumerable<Category>> GetByIds(IEnumerable<int> ids, bool trackChanges);
        void DeleteCategory(Category cat);
    }
}
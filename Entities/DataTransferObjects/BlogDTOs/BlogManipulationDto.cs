using System.ComponentModel.DataAnnotations;

namespace Entities_.DataTransferObjects.BlogDTOs
{
    public abstract class BlogManipulationDto
    {
        [Required(ErrorMessage = "Blog title is a required field")]
        [MaxLength(60,ErrorMessage = "Maximum length for the Title is 100 characters.")]
        public string Title { get; set; }
        
        [Required(ErrorMessage = "Blog description is a required field")]
        [MaxLength(260,ErrorMessage = "Maximum length for the description is 260 characters.")]
        public string Description { get; set; }
        
        [Required(ErrorMessage = "Blog content is a required field")]
        public string Content { get; set; }
    }
}
using System;
using Entities_.DataTransferObjects.CategoryDTOs;

namespace Entities_.DataTransferObjects.BlogDTOs
{
    public class BlogDto
    {
        public int Id{ get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
        public int CountLikes { get; set; }
        public CategoryDto Category { get; set; }
    }
}
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Entities_.DataTransferObjects.BlogDTOs;

namespace Entities_.DataTransferObjects.CategoryDTOs
{
    public class CategoryForCreationDto:CategoryManipulationDto
    {
        public IEnumerable<BlogForCreactionDto> Blogs { get; set; }
    }
}
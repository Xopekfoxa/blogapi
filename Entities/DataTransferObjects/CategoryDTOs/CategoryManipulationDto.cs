using System.ComponentModel.DataAnnotations;

namespace Entities_.DataTransferObjects.CategoryDTOs
{
    public abstract class CategoryManipulationDto
    {
        [Required(ErrorMessage = "Category name is a required field")]
        [MaxLength(60,ErrorMessage = "Maximum length for the Name is 60 characters.")]
        public string Name { get; set; }
    }
}
namespace Entities_.RequestFeatures
{
    public class BlogParameters : RequestParameters
    {
        public BlogParameters()
        {
            OrderBy = "title";
        }
        public string SearchTerm { get; set; }
    }
}
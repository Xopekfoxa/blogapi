using System;
using Entities_.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Entities_.Configuretion
{
    public class CategoryConfiguration: IEntityTypeConfiguration<Category>
    {
        public void Configure(EntityTypeBuilder<Category> builder)
        {
            builder.HasData(
                new 
                {
                    Id = 1,
                    Name = "Sport"
                }
                ,new 
                {
                    Id = 2,
                    Name = "Design"
                });
        }
    }
}
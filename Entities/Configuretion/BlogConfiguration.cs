using System;
using System.Security.Cryptography.X509Certificates;
using Entities_.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Entities_.Configuretion
{
    public class BlogConfiguration : IEntityTypeConfiguration<Blog>
    {
        public void Configure(EntityTypeBuilder<Blog> builder)
        {
        
            builder.HasData(
                new Blog
                {
                    Id = 1,
                    Title = "Darova",
                    Description = "I I I I I I I I I I I I",
                    Content = "Lorem LoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLorem",
                    CategoryId = 1,
                    DateCreated = DateTime.Now,
                    DateUpdated = DateTime.Now,
                    CountLikes = 0
                },
                new Blog
                {
                    Id = 2,
                    Title = "Darova #2",
                    Description = "I I I I I I I I I I I I",
                    Content = "Lorem LoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLorem",
                    CategoryId = 2,
                    DateCreated = DateTime.Now,
                    DateUpdated = DateTime.Now,
                    CountLikes = 0
                });
        }
    }
}
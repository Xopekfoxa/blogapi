using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities_.Models
{
    public class Category
    {
        [Key]
        [Column("CategoryId")]
        public int Id { get; set; }
        
        [Required(ErrorMessage = "Category name is a required field")]
        [MaxLength(60,ErrorMessage = "Maximum length for the Name is 60 characters.")]
        public string Name { get; set; }
        
        public ICollection<Blog> Blogs { get; set; }
    }
}
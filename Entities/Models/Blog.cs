using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities_.Models
{
    public class Blog
    {
        public Blog()
        {
            DateCreated = DateTime.Now;
            DateUpdated = DateTime.Now;
            CountLikes = 0;
        }
        [Key]
        [Column("BlogId")]
        public int Id { get; set; }
          
        [Required(ErrorMessage = "Blog title is a required field")]
        [MaxLength(60,ErrorMessage = "Maximum length for the Title is 100 characters.")]
        public string Title { get; set; }
          
        [Required(ErrorMessage = "Blog description is a required field")]
        [MaxLength(260,ErrorMessage = "Maximum length for the description is 260 characters.")]
        public string Description { get; set; }
        
        [Required(ErrorMessage = "Blog content is a required field")]
        public string Content { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateUpdated { get; set; }
        
        public int CountLikes { get; set; }

        [ForeignKey(nameof(Category))]
        public int CategoryId { get; set; }
        public Category Category { get; set; }
    }
}